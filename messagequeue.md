#         MESSAGE QUEUE     
 Message queuing allows applications to communicate by sending the messages to each other. The message queue is a temporary storage which stores the necessary when the destination server is busy or not connected.Message queues helps a lot with ensuring that the  data is never lost.
   
  
 ### The  purpose of  choosing Message Queue:

1. Redundancy   
1.  Traffic Spikes
1. Batch  
1. Asynchronous Messaging  
1. Transaction Ordering  
1. Improve Scalability  
1. Guarantee Transaction Occurs Once  
1.  Break Larger Tasks into Many Smaller Ones  
1. Monitoring   
1. Message-driven processing  
1. Event-driven processing   
1. Time-independent communication


### 1.Redundancy   :
Queues is used for confirming that the process is completed and  the transaction is safe to remove it. If anything fails in the worst case scenario, the message is stored somewhere and won’t be lost for it to be processed later.  
### 2. Traffic Spikes:
We will always not know  how much traffic is our data is going through. For example, a particular website or mail system will have  millions of messages to be processed. SoSwetha53@
 by queueing the data ,we can store and then take it to be processed.   
### 3. Batch :  
With the help of Batch method we can insert hundreds of messages(data) at a time in to  elasticsearch and SQL Server. Batch helps us optimize the performance by tuning the size of the transactions.
   
### 4.Asynchronous Messaging :
Queues paly an important role in Asynchronous messaging,suppose when a data has to processed is of low priority .Then it is stored in the data base and processed later.  
### 5.Transaction Ordering :
Suppose if 1000 people are placing an order to a  website in the same time, accodrding to the time they requested it .So with the help od queuing ,the number of processes are aligned in a queue and then it is processed.  
### 6. Improve Scalability  :
Message queues helps to  decouple different parts of your application and then scale them independently. Using Azure, AWS, or other hosting solutions you could even dynamically scale that background service based on CPU usage or other metrics. Message queues can help a lot with scalability and elasticity.
   
### 7. Guarantee Transaction Occurs Once  :
When a message or data is taken from the queue ,we can inform in which server it is processed and then the data gets removed from the queue. So this ensures,that the data is processed only once and there is no duplication.  
### 8. Break Larger Tasks into Many Smaller Ones  :
Queues are capable of breaking  a larger task into lots of little pieces and then queuing them all up. So this is helpful in processing the data at higher rate .  
### 9.Monitoring :
With the help of Message queuing we can  monitor how many items are there in a queue processing  rate . This is also helpful to monitor how the data is flowing through the system.  
### 10.Message-driven processing  :
When messages arrive on a queue, they can automatically start an application using triggering. If necessary, the applications can be stopped when the message (or messages) have been processed.   
### 11.Event-driven processing :
Programs can be controlled according to the state of queues. For example, you can arrange for a program to start as soon as a message arrives on a queue, or you can specify that the program does not start until there are, for example, 10 messages above a certain priority on the queue, or 10 messages of any priority on the queue.   
### 12.Time-independent communication:
Programs requesting others to do work do not have to wait for the reply to a request. They can do other work, and process the reply either when it arrives or at a later time. When writing a messaging application, we need not know  whether a program sends a message, or whether the target is able to receive the message. The message is not lost; it is retained by the queue manager until the target is ready to process it. The message stays on the queue until it is removed by a program. This means that the sending and receiving application programs are decoupled; the sender can continue processing without waiting for the receiver to acknowledge receipt of the message. The target application does not even have to be running when the message is sent. It can retrieve the message after it is has been started.    
### The benefits of using Message Queue are :  
**<font size ='4'> 1. Reliable message delivery </font>**:Using a message queue  we can ensure that confidential messages between applications will not be lost and that they will be only be delivered to the recipient once.  
**<font size ='4'> 2.Versatility:</font>**  Message queue solutions can support multiple languages, such as Java, Node.js, COBOL, C/C++, Go, .NET, Python, Ruby, and C#. They can also support numerous application programing interfaces (APIs) and protocols, including MQTT, AMQP, and REST.   
**<font size ='4'> 3.Resilience: </font>**  Asynchronous messaging ensures that if specific application [server or device] faults, it  won’t impact the system. If one component in the system falls, all other component [server or devices]can continue interacting with the queue and process the  messages. This decreases the chance that the entire system’s stability will be impacted by one part’s failure.   
**<font size ='4'> 4.Improved security:  </font>** A message queue may be able to identify and authenticate all messages, and in some message queue solutions, they can be set to encrypt messages at rest, in transit, or end-to-end.   
### The most popularly used Messege queues are:  
**<font size ='4'>1. Apache Kafka — Robust Queue Broker :</font>** 
The Kafka  message is used in real time to store the data is queues.This  is an open source messaging system and a robust queue broker. Kafka has the ability to handle  high volume of messages. The kafka allows the messeges to be stored on the disk and also allows to send messages from one point to another seamlessly. Apache message queue messages are replicated within the whole Kafka cluster to prevent the unwanted  data loss. 
This message queuing  software is used by thousands of companies because of high performance data pipelines and the integration with Apache Storm and Spark.  Apache Kafka queue is an alternative to a variety of enterprise messaging systems. It is built in a way that it can   handle 1.4 trillion messages in a day. Kafka message queue is a best and suitable platform for the implementation of Queues because it boosts performance by using sequential disk I/O operations. It can also be used in a place where there are numerous of data to be processed .Tgives an advantage of providing high throughput with limited number of resources, i.e millions of messages per second.  

**<font size ='4'>2. RabbitMQ :</font>** Robust Messaging for Applications
RabbitMQ is the  popular open source best message broker software . RabbitMQ works on  Erlang programming language . It gives your applications a common platform and a safe place to send and receive messages. The features of RabbitMQ includes
 performance
 reliability,
high availability
 RabbitMQ has an ease of use User Interface ,so that the user can eaisly monitor and control the message broker.
RabbitMQ message broker  can be eaisly downloaded from the official website . It is recommended to use RabbitMQ message queuing service plugins to ease the workload of top message brokers and to expand it’s functions.  RabbitMQ message broker open source management plugin helps users to operate RabbitMQ using it through a Graphical User Interface. It helps in viewing different statistics related to messaging and also keeping an overview of all of the operations with data happening in queues.  

**<font size ='4'>3. Celery — Distributed Task Queue:</font>**
Celery  is an open source Message queue which supports  task scheduling. It is suited for working on real time.This Message queue has a simple asynchronous process queue or job queue which is based on distributed message passing. The execution units are executed concurrently on a single or more worker nodes using multiprocessing. Celery tasks run asynchronously in the background or synchronously.
Celery is written using Python but the protocol can be implemented in any language. Celery is a best message queue for micro services, used in production systems like for Instagram, to process millions of tasks every day. It can also work with other programming languages using webhooks. There is a PHP client, Go client, a Node.js client and a Ruby-Client called RCelery. Celery is an open source message queue with 17.6K GitHub stars and 4K GitHub forks.  

**<font size ='4'>4. Nsq — Realtime Distributed Messaging :</font>**
NSQ is an open source and modern real-time distributed memory best message queue designed to operate at scale. It is written in Go language and handles billions of messages for one day on a large scale. NSQ message queue notification system has distributed message and decentralized topology structure. . Itt has fault tolerance capability  .
NSQ is mature product, easy to configure and has excellent performance. All configuration and deployment parameters are specified on the command line and compiled binaries have no runtime dependencies. NSQ messages data format can be JSON, MsgPack, Protocol Buffers, or anything else for maximum flexibility. It has official Go and Python libraries out of the box as well as many other client libraries.   

**<font size ='4'>5. Redisson :</font>**
Redisson  is the most advanced and easiest Redis Java client with features of In-Memory data grid. It is very simple, easy to learn and message queue monitoring tool the user no need to know any Redis commands to start configuring with Redisson. It is required Redis based objects, collections, locks, synchronizers and services for distributed applications on Java platform. Task services on Java might be run in parallel with Redis based distributed implementations with ExecutorService and ScheduledExecutorService.    
###  Enterprise Service Bus (ESB) 
An enterprise service bus (ESB) is a software platform used for  distributing the  work among connected components of an application. It is designed in a way to provide an uniform means of moving work.
#### Key principles:
The ESB architecture has some key principles that allow for business agility and scale.   

   • The "bus" concept decouples applications from each other. This is usually achieved using a messaging server like JMS or AMQP.  

   • The data that travels on the bus is a canonical format and is almost always XML.  

• There is an "adapter" between the application and the bus that marshals data between the two parties.  

 • The adapter is responsible for talking to the backend application and transforming data from the application format to the bus format. The adapter can also perform a host of other activities such as message routing transaction management,security,monitoring, error handling, etc.  

 • ESBs are generally stateless; the state is embedded in the messages passing through the bus.  

  • The canonical message format is the contract between systems. The canonical format means that there is one consistent message format traveling on the bus and that every application on the bus can communicate with each other.
  

##### Reference Links:   
 https://www.ibm.com/docs/en/ibm-mq/9.2?topic=queuing-main-features-benefits-message    
https://www.cloudamqp.com/blog/what-is-message-queuing.html







    
  




  




          
 
                                   